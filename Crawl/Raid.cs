using FlatData;
using MX.NetworkProtocol;
using Newtonsoft.Json;

namespace Crawl
{
    public static class Raid
    {
        public static void SaveRankList(int rankFrom, int rankTo, string outputFolder = "Raid")
        {
            string outFolder = Path.Combine(Settings.saveFolder, outputFolder);
            Directory.CreateDirectory(outFolder);
            int rank = int.Max(1, rankFrom + 15);
            int stop = int.Max(rank + 1, rankTo + 15);
            for (; rank < stop; rank += 30)
            {
                RaidOpponentListResponse res = RankQuery(rank);
                var responseFilePath = Path.Combine(
                    outFolder,
                    $"Raid_rank{rank - 15:D8}_{rank + 14:D8}.json"
                );
                string str = JsonConvert.SerializeObject(res);
                File.WriteAllText(responseFilePath, str);
                Thread.Sleep(1000);
            }
            Console.WriteLine($"Files saved to {outFolder}");
        }

        public static RaidOpponentListResponse RankQuery(long rank)
        {
            RaidOpponentListRequest packet =
                new()
                {
                    IsFirstRequest = true,
                    SearchType = RankingSearchType.Rank,
                    ClientUpTime = 12,
                    SessionKey = Settings.session,
                    Rank = rank
                };
            string res = Settings.api.Post(packet);
            var obj = JsonConvert.DeserializeObject<RaidOpponentListResponse>(res);
            ArgumentNullException.ThrowIfNull(obj);
            return obj;
        }

        public static RaidOpponentListResponse ScoreQuery(long score)
        {
            RaidOpponentListRequest packet =
                new()
                {
                    IsFirstRequest = true,
                    SearchType = RankingSearchType.Score,
                    ClientUpTime = 12,
                    SessionKey = Settings.session,
                    Score = score
                };
            string res = Settings.api.Post(packet);
            var obj = JsonConvert.DeserializeObject<RaidOpponentListResponse>(res);
            ArgumentNullException.ThrowIfNull(obj);
            return obj;
        }

        public static long FetchBestRankingPoint(long rank)
        {
            RaidOpponentListResponse res = RankQuery(rank);
            return res.OpponentUserDBs.First().BestRankingPoint;
        }

        public static long FetchRank(long score)
        {
            RaidOpponentListResponse res = ScoreQuery(score);
            return res.OpponentUserDBs.First().Rank;
        }
    }
}
