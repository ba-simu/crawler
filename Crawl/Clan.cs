using MX.NetworkProtocol;

namespace Crawl
{
    public static class Clan
    {
        public static void SaveMemberList(long clanDBId, string outputFolder = "Clan")
        {
            string outFolder = Path.Combine(Settings.saveFolder, outputFolder);
            Directory.CreateDirectory(outFolder);

            ClanMemberListRequest packet =
                new()
                {
                    ClientUpTime = 12,
                    SessionKey = Settings.session,
                    ClanDBId = clanDBId
                };
            string res = Settings.api.Post(packet);
            var responseFilePath = Path.Combine(outFolder, $"{clanDBId}.json");
            File.WriteAllText(responseFilePath, res);
            Console.WriteLine($"Files saved to {outFolder}");
        }
    }
}
