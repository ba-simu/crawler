using MX.NetworkProtocol;
using Newtonsoft.Json;

namespace Crawl
{
    public class RaidLobby
    {
        public RaidLobbyResponse RaidLobbyQuery()
        {
            RaidLobbyRequest packet = new() { ClientUpTime = 12, SessionKey = Settings.session };
            string res = Settings.api.Post(packet);
            var obj = JsonConvert.DeserializeObject<RaidLobbyResponse>(res);
            ArgumentNullException.ThrowIfNull(obj);
            return obj;
        }
    }
}
