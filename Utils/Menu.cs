namespace Utils
{
    public class Menu
    {
        public static bool MainMenu()
        {
            Console.Clear();
            Console.WriteLine("1) 查询社团人员");
            Console.WriteLine("2) 爬取总力榜单");
            Console.WriteLine("3) 爬取大决战榜单");
            Console.WriteLine("4) 查询总力战档线和各难度人数");
            Console.WriteLine("5) 退出");
            Console.Write("\r\n选择序号:");

            switch (Console.ReadLine())
            {
                case "1":
                    Clan();
                    return true;
                case "2":
                    Raid();
                    return true;
                case "3":
                    ERaid();
                    return true;
                case "4":
                    TierAndDiff();
                    return true;
                case "5":
                    return false;
                default:
                    return true;
            }
        }

        private static void CompleteExecute()
        {
            Console.WriteLine("操作完成，按任意键继续...");
            Console.ReadKey();
        }

        private static void Clan()
        {
            Console.Clear();
            long clanDBId = GetLongInput("请输入社团序号:");
            Crawl.Clan.SaveMemberList(clanDBId);
            CompleteExecute();
        }

        private static void Raid()
        {
            var rankRange = GetRankRange();
            Crawl.Raid.SaveRankList(rankRange.rankFrom, rankRange.rankTo);
            CompleteExecute();
        }

        private static void ERaid()
        {
            var rankRange = GetRankRange();
            Crawl.ERaid.SaveRankList(rankRange.rankFrom, rankRange.rankTo);
            CompleteExecute();
        }

        private static void TierAndDiff()
        {
            Crawl.RaidTier.TierAndDifficultyPeople();
            CompleteExecute();
        }

        private static (int rankFrom, int rankTo) GetRankRange()
        {
            Console.Clear();
            int rankFrom = GetIntInput("请输入起始排名:");
            int rankTo = GetIntInput($"你设置的起始排名为 {rankFrom}，请输入结束排名:");

            while (rankTo < rankFrom)
            {
                Console.Clear();
                Console.WriteLine("结束排名不可小于起始排名!");
                rankTo = GetIntInput($"你设置的起始排名为 {rankFrom}，请输入结束排名:");
            }

            return (rankFrom, rankTo);
        }

        private static int GetIntInput(string prompt)
        {
            int input;
            Console.Write(prompt);
            while (!int.TryParse(Console.ReadLine(), out input))
            {
                Console.WriteLine("请输入正确的数字!");
                Console.Write(prompt);
            }
            return input;
        }

        private static long GetLongInput(string prompt)
        {
            long input;
            Console.Write(prompt);
            while (!long.TryParse(Console.ReadLine(), out input))
            {
                Console.WriteLine("请输入正确的数字!");
                Console.Write(prompt);
            }
            return input;
        }
    }
}
