using DotNetEnv;
using MX.GameLogic.DBModel;
using Utils.Rest;

public static class Settings
{
    public static readonly SessionKey session;
    public static readonly API api;
    public static readonly string saveFolder;

    static Settings()
    {
        if (File.Exists(".env"))
            Env.Load(".env");
        if (File.Exists(".secret"))
            Env.Load(".secret");

        session = GetSession();
        api = GetApi();
        saveFolder = Path.Combine(AppContext.BaseDirectory, "output");
        Directory.CreateDirectory(saveFolder);
    }

    static SessionKey GetSession()
    {
        string? AccountServerId = Environment.GetEnvironmentVariable("AccountServerId");
        ArgumentNullException.ThrowIfNullOrWhiteSpace(AccountServerId);
        string? MxToken = Environment.GetEnvironmentVariable("MxToken");
        ArgumentNullException.ThrowIfNullOrWhiteSpace(MxToken);
        return new SessionKey()
        {
            AccountServerId = long.Parse(AccountServerId),
            MxToken = MxToken
        };
    }

    static API GetApi()
    {
        string serverString = Environment.GetEnvironmentVariable("Server") ?? "";
        Server server = (Server)Enum.Parse(typeof(Server), serverString);
        return new API(server);
    }
}
